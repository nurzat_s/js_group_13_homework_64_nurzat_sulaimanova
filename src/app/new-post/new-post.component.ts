import { Component, ElementRef, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-new-post',
  templateUrl: './new-post.component.html',
  styleUrls: ['./new-post.component.css']
})
export class NewPostComponent {
  @ViewChild('titleInput') titleInput!: ElementRef;
  @ViewChild('descriptionInput') descriptionInput!: ElementRef;

  constructor(public http: HttpClient) { }

  createPost() {
    const title = this.titleInput.nativeElement.value;
    const description = this.descriptionInput.nativeElement.value;

    const dateCreated = new Date();

    const body = {title, dateCreated: dateCreated.toString(), description};
    this.http.post('https://plovo-e531e-default-rtdb.firebaseio.com/posts.json', body).subscribe();
  }
}
