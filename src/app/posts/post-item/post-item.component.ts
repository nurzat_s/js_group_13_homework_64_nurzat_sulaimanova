import { Component, Input } from '@angular/core';
import { Post } from '../../shared/post.model';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-post-item',
  templateUrl: './post-item.component.html',
  styleUrls: ['./post-item.component.css']
})
export class PostItemComponent {
  @Input() post!: Post;

  constructor(private http: HttpClient) {}


  onClick() {

  }
}
