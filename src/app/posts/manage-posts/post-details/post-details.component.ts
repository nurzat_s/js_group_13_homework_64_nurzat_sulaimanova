import { Component, OnInit } from '@angular/core';
import { Post } from '../../../shared/post.model';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-post-details',
  templateUrl: './post-details.component.html',
  styleUrls: ['./post-details.component.css']
})
export class PostDetailsComponent implements OnInit {
  post!: Post;
  postId: number | null = null;



  constructor(private route: ActivatedRoute, private http: HttpClient, private router: Router) { }

  ngOnInit(): void {

    this.route.params.subscribe((params: Params) => {
      if(params['id']) {
        const postId = params['id'];
        this.postId = postId;
        this.http.get<Post>(`https://plovo-e531e-default-rtdb.firebaseio.com/posts/${postId}.json`)
          .pipe(map(result => {
            return new Post(result.id, result.title, result.dateCreated, result.description);
          }))
          .subscribe(post => {
            this.post = post;
          });
      }
    })
  }

  delete() {
    this.route.params.subscribe((params: Params) => {
      if(params['id']) {
        const postId = params['id'];
        this.postId = postId;
        this.http.delete<Post>(`https://plovo-e531e-default-rtdb.firebaseio.com/posts/${postId}.json`)
          .pipe(map(result => {
            return new Post(result.id, result.title, result.dateCreated, result.description);
          }))
          .subscribe(post => {
            this.post = post;
          });
        void this.router.navigate(['/']);
      }
    })
  }

}

