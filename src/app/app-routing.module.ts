import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PostsComponent } from './posts/posts.component';
import { NewPostComponent } from './new-post/new-post.component';
import { PostDetailsComponent } from './posts/manage-posts/post-details/post-details.component';
import { AboutComponent } from './about/about.component';
import { ContactsComponent } from './contacts/contacts.component';


const routes: Routes = [
  {path: '', component: PostsComponent},
  {path: 'posts', component: PostsComponent},
  {path: 'new', component: NewPostComponent},
  {path: 'about', component: AboutComponent},
  {path: 'contacts', component: ContactsComponent},
  {path: ':id', component: PostDetailsComponent},
  {path: ':id/edit', component: NewPostComponent},

]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})


export class AppRoutingModule {}
